<?php

/**
 * @file
 * Provides an integrated sign up flow for multiple merchants on a site.
 */


/**
 * This function creates the admin settings form for the module.
 */
function paypal_registration_integration_admin() {
  $form = array();

  $form['service_description'] = array(
    '#markup' => '<div><h2>'
      . t("Paypal Registration Integration Settings") . '</h2><br />'
      . t("Your Merchant Account ID can be found by logging into your paypal account and going to Profile -> My Business Info.") . '</div>',
  );

  $form['paypal_registration_integration_partnerId'] = array(
    '#type' => 'textfield',
    '#title' => t('Secure Merchant ID'),
    '#default_value' => variable_get('paypal_registration_integration_partnerId', ""),
    '#required' => FALSE,
  );

  $form['paypal_registration_integration_server'] = array(
    '#type' => 'radios',
    '#title' => t('PayPal Registration server'),
    '#options' => array(
      'sandbox' => ('Sandbox - use for testing, requires a PayPal Sandbox account'),
      'live' => ('Live - use for processing real sign-ups'),
    ),
    '#default_value' => variable_get('paypal_registration_integration_server', 'sandbox'),
  );

  $form['paypal_registration_integration_experience'] = array(
    '#type' => 'radios',
    '#title' => t('PayPal Registration Experience'),
    '#options' => array(
      'regular' => ('Full Page - redirected to Paypal'),
      'minibrowser' => ('Mini Browser - small window on top of current page'),
    ),
    '#default_value' => variable_get('paypal_registration_integration_experience', 'minibrowser'),
  );

  $form['paypal_registration_integration_permissions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select the Permissions the Merchants will be Granting the Partner'),
    '#options' => array(
      'EXPRESS_CHECKOUT' => t('EXPRESS_CHECKOUT'),
      'REFUND' => t('REFUND'),
      'AUTH_CAPTURE' => t('AUTH_CAPTURE'),
      'TRANSACTION_DETAILS' => t('TRANSACTION_DETAILS'),
      'TRANSACTION_SEARCH' => t('TRANSACTION_SEARCH'),
      'REFERENCE_TRANSACTION' => t('REFERENCE_TRANSACTION'),
      'RECURRING_PAYMENTS' => t('RECURRING_PAYMENTS'),
      'ACCOUNT_BALANCE' => t('ACCOUNT_BALANCE'),
      'DIRECT_PAYMENT' => t('DIRECT_PAYMENT'),
      'BUTTON_MANAGER' => t('BUTTON_MANAGER'),
      'NON_REFERENCED_CREDIT' => t('NON_REFERENCED_CREDIT'),
    ),
    '#default_value' => variable_get('paypal_registration_integration_permissions', array(
        'EXPRESS_CHECKOUT' => 0,
        'REFUND' => 0,
        'AUTH_CAPTURE' => 0,
        'TRANSACTION_DETAILS' => 0,
        'TRANSACTION_SEARCH' => 0,
        'REFERENCE_TRANSACTION' => 0,
        'RECURRING_PAYMENTS' => 0,
        'ACCOUNT_BALANCE' => 0,
        'DIRECT_PAYMENT' => 0,
        'BUTTON_MANAGER' => 0,
        'NON_REFERENCED_CREDIT' => 0
      )
    ),
  );

  return system_settings_form($form);
}

/**
 * This function creates the URL and page that hosts the admin settings.
 */
function paypal_registration_integration_menu() {
  $items = array();

  $items['admin/config/paypal/registration_integration'] = array(
    'title' => 'Paypal Registration',
    'description' => 'Settings for integrating the Paypal Sign Up flow.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('paypal_registration_integration_admin'),
    'access arguments' => array('administer paypal_registration_integration settings'),
    'type' => MENU_NORMAL_ITEM,
   );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function paypal_registration_integration_block_info() {
  $blocks = array();
  $blocks['paypal_sign_up'] = array(
    'info' => t('Paypal Sign Up'),
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function paypal_registration_integration_block_view($delta='') {
  $block = array();

  switch($delta) {
    case 'paypal_sign_up' :
      $block['subject'] = t('<none>');
      $block['content'] = '
        <div dir="ltr" style="text-align: left;" trbidi="on">
        <script>
          (function(d, s, id){
            var js, ref = d.getElementsByTagName(s)[0];
            if (!d.getElementById(id)){
              js = d.createElement(s); js.id = id; js.async = true;
              js.src = "https://www.paypal.com/webapps/merchantboarding/js/lib/lightbox/partner.js";
              ref.parentNode.insertBefore(js, ref);
            }
          }(document, "script", "paypal-js"));
        </script>
        <a data-paypal-button="true" href="' . _paypal_registration_integration_get_sign_up_url() . '" target="PPFrame">Sign up for PayPal </a>
        </div>';
      break;
  }

  return $block;
}

function _paypal_registration_integration_get_sign_up_url() {
  $url = _paypal_registration_integration_get_server()
    . '/webapps/merchantboarding/webflow/externalpartnerflow?partnerId='
    . variable_get('paypal_registration_integration_partnerId')
    . '&productIntentID=addipmt&integrationType=T&permissionNeeded='
    . _paypal_registration_integration_get_permissions()
    . '&returnToPartnerUrl=' . $GLOBALS['base_url'] . '/returnurl'
    . '&displayMode='
    . variable_get('paypal_registration_integration_experience', 'minibrowser')
    . '&receiveCredentials=true&showPermissions=true';

  return $url;
}

function _paypal_registration_integration_get_server() {
  $server = variable_get('paypal_registration_integration_server', 'sandbox');
  if ($server == 'live') {
    $url = 'https://www.paypal.com';
  }
  else {
    $url = 'https://www.sandbox.paypal.com';
  }

  return $url;
}

function _paypal_registration_integration_get_permissions() {
  $resultstr = array();
  $permissions = variable_get('paypal_registration_integration_permissions');

  foreach ($permissions as $permission) {
    if ($permission !== 0) {
      $resultstr[] = $permission;
    }
  }
  // add commas between results
  $result = implode(",",$resultstr);

  return $result;
}
